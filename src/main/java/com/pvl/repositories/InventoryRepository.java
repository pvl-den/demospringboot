package com.pvl.repositories;

import com.pvl.entity.Inventory;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by denis on 27.09.2017.
 */
public interface InventoryRepository extends CrudRepository<Inventory,Long> {

}
