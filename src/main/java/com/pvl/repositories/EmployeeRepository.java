package com.pvl.repositories;

import com.pvl.entity.Employee;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by denis on 27.09.2017.
 */
public interface EmployeeRepository extends CrudRepository<Employee,Long> {
}
