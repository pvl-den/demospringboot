package com.pvl.controller;

import com.pvl.entity.Employee;
import com.pvl.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by denis on 27.09.2017.
 */
@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;


    @RequestMapping(value = "/employees",  method = RequestMethod.GET)
    @ResponseBody
    public List<Employee> getListEmployee(){
        return employeeService.getAllEmployee();
    }

    @RequestMapping(value = "/employees",  method = RequestMethod.POST)
    @ResponseBody
    public Employee addEmployee(@RequestBody Employee employee){
        return employeeService.addEmployee(employee);
    }

    @RequestMapping(value = "/employees",  method = RequestMethod.PUT)
    @ResponseBody
    public Employee updateEmployee(@RequestBody Employee employee){
        return employeeService.updateEmployee(employee);
    }

    @RequestMapping(value = "/employees{id}",  method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteEmployee(@PathVariable long id){
        employeeService.deleteEmployee(id);
    }


}
