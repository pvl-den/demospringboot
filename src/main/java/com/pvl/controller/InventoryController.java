package com.pvl.controller;

import com.pvl.entity.Inventory;
import com.pvl.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by denis on 27.09.2017.
 */
@RestController
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;


    @RequestMapping(value = "/inventory",  method = RequestMethod.GET)
    @ResponseBody
    public List<Inventory> getListInventory(){
        return inventoryService.getAllInventory();
    }

    @RequestMapping(value = "/inventory",  method = RequestMethod.POST)
    @ResponseBody
    public Inventory addInventory(@RequestBody Inventory inventory){
        return inventoryService.addInventory(inventory);
    }

    @RequestMapping(value = "/inventory",  method = RequestMethod.PUT)
    @ResponseBody
    public Inventory updateInventory(@RequestBody Inventory inventory){
        return inventoryService.updateInventory(inventory);
    }

    @RequestMapping(value = "/inventory{id}",  method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteEmployee(@PathVariable long id){
        inventoryService.deleteInventory(id);
    }

}
