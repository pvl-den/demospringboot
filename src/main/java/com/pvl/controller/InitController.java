package com.pvl.controller;

import com.pvl.entity.Employee;
import com.pvl.entity.Inventory;
import com.pvl.service.EmployeeService;
import com.pvl.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by denis on 27.09.2017.
 */
@RestController
public class InitController {

    @Autowired
    private InventoryService inventoryService;
    @Autowired
    private EmployeeService employeeService;



    //инициализация БД
    @RequestMapping(value = "/initBD")
    public String initBD() {
        List<Inventory> inventories = Stream.of(new Inventory("Монитор1",101),new Inventory("Стол1",201)).collect(Collectors.toList());
        inventories.stream().forEach(x -> inventoryService.addInventory(x));


        List<Employee> employees = Stream.of(new Employee("Jack","Daniels","USA","developer",inventories)).collect(Collectors.toList());
        employees.stream().forEach(x -> employeeService.addEmployee(x));

        return "Done";
    }


}
