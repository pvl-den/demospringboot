package com.pvl.service;

import com.pvl.entity.Employee;

import java.util.List;

/**
 * Created by denis on 27.09.2017.
 */
public interface EmployeeService {

    Employee addEmployee(Employee employee);
    void deleteEmployee(long id);
    Employee updateEmployee(Employee employee);
    List<Employee> getAllEmployee();

}
