package com.pvl.service;

import com.pvl.entity.Employee;
import com.pvl.entity.Inventory;

import java.util.List;

/**
 * Created by denis on 27.09.2017.
 */
public interface InventoryService {
    Inventory addInventory(Inventory inventory);
    void deleteInventory(long id);
    Inventory updateInventory(Inventory inventory);
    List<Inventory> getAllInventory();
}
