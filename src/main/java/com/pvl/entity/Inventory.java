package com.pvl.entity;

import javax.persistence.*;

/**
 * Created by denis on 27.09.2017.
 */
@Entity
@Table(name = "inventory")
public class Inventory {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name_inv")
    private String nameInv;

    @Column(name = "price_inv")
    private long priceInv;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameInv() {
        return nameInv;
    }

    public void setNameInv(String nameInv) {
        this.nameInv = nameInv;
    }

    public long getPriceInv() {
        return priceInv;
    }

    public void setPriceInv(long priceInv) {
        this.priceInv = priceInv;
    }

    public Inventory() {
    }

    public Inventory(String nameInv, long priceInv) {
        this.nameInv = nameInv;
        this.priceInv = priceInv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Inventory inventory = (Inventory) o;

        if (id != inventory.id) return false;
        if (priceInv != inventory.priceInv) return false;
        return nameInv != null ? nameInv.equals(inventory.nameInv) : inventory.nameInv == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (nameInv != null ? nameInv.hashCode() : 0);
        result = 31 * result + (int) (priceInv ^ (priceInv >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "id=" + id +
                ", nameInv='" + nameInv + '\'' +
                ", priceInv=" + priceInv +
                '}';
    }
}
